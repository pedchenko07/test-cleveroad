const path = require('path');
const { DataTypes } = require('sequelize');
const connection = require('../connection');

const modelName = path.basename(__filename, '.js');

const model = connection.define(
  modelName,
  {
    id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
    email: DataTypes.STRING,
    name: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: true,
      },
    },
    password: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: true,
      },
    },
    phone: {
      type: DataTypes.STRING,
    },
    created_at: DataTypes.DATE,
    updated_at: DataTypes.DATE,
  },
  {
    tableName: modelName,
    underscored: true,
  },
);

module.exports = model;
