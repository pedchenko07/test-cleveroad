const path = require('path');
const { DataTypes } = require('sequelize');
const connection = require('../connection');
const User = require('./User');

const modelName = path.basename(__filename, '.js');

const model = connection.define(
  modelName,
  {
    id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
    title: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: true,
      },
    },
    price: {
      type: DataTypes.FLOAT,
      validate: {
        isFloat: true,
        notEmpty: true,
      },
    },
    image: {
      type: DataTypes.STRING,
    },
    user_id: {
      type: DataTypes.INTEGER,
    },
    created_at: DataTypes.DATE,
    updated_at: DataTypes.DATE,
  },
  {
    tableName: modelName,
    underscored: true,
  },
);

model.belongsTo(User, { as: 'user', foreignKey: 'user_id' });

module.exports = model;
