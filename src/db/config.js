require('dotenv').config();

const { DB_USERNAME, DB_NAME, DB_PASS, DB_HOST } = process.env;

module.exports = {
  username: DB_USERNAME,
  password: DB_PASS,
  database: DB_NAME,
  host: DB_HOST,
  dialect: 'mysql',
};
