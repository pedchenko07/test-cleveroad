class Migration {
  static get table() {
    return 'User';
  }

  static async up(queryInterface, Sequelize) {
    await queryInterface.createTable(Migration.table, {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      email: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      name: {
        type: Sequelize.STRING,
      },
      password: {
        type: Sequelize.STRING,
      },
      phone: {
        type: Sequelize.STRING,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });

    await queryInterface.addIndex(Migration.table, {
      fields: ['email'],
      unique: true,
    });

    await queryInterface.addIndex(Migration.table, {
      fields: ['phone'],
      unique: true,
    });

    return null;
  }

  static async down(queryInterface) {
    await queryInterface.dropTable(Migration.table, {});
    return null;
  }
}

module.exports = Migration;
