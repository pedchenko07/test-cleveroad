const Sequelize = require('sequelize');

const config = require('./config');
const test = require('./test');

const connection = new Sequelize(config);

test(connection);

module.exports = connection;
