require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const { apiRouter, userRouter, itemRouter } = require('./routers');

const app = express();
const { APP_PORT = 3000 } = process.env;

app.use('/public', express.static('public'));
app.use(bodyParser.json());
app.use('/api', apiRouter);
app.use('/api/user', userRouter);
app.use('/api/item', itemRouter);

app.listen(APP_PORT, () => {
  // eslint-disable-next-line no-console
  console.log(`App listening on port ${APP_PORT}!`);
});
