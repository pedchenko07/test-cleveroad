const jwtService = require('jsonwebtoken');

const { JWT_SECRET } = process.env;

class JwtService {
  static generateToken(id) {
    return jwtService.sign({ id }, JWT_SECRET);
  }
}

module.exports = JwtService;
