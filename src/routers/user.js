const { Router } = require('express');
const { jwtMiddleware } = require('../middleware');
const { User } = require('../models');

const router = new Router();

router.get('/:id', jwtMiddleware, async (req, res) => {
  const { id } = req.params;
  const user = await User.findById(id);
  if (!user) {
    res.status(404).send();
    return res.end();
  }

  res.send({ user });
  return res.end();
});

router.get('/', async (req, res) => {
  const { query } = req;
  const users = await User.findAll({ where: query });

  res.send(users);
  return res.end();
});

module.exports = router;
