const apiRouter = require('./api');
const userRouter = require('./user');
const itemRouter = require('./item');

module.exports = {
  apiRouter,
  userRouter,
  itemRouter,
};
