const { Router } = require('express');
const { hashSync, compareSync } = require('bcryptjs');
const { User } = require('../models');
const { JwtService } = require('../services');
const {
  jwtMiddleware,
  registerValidation,
  loginValidation,
  userUpdateValidation,
} = require('../middleware');

const router = Router();

router.post('/login', loginValidation, async (req, res) => {
  const { email, password } = req.body;
  const user = await User.findOne({ where: { email } });

  if (!user || !compareSync(password, user.password)) {
    res.status(422).send({ message: 'Wrong email or password' });
    return res.end();
  }

  const token = JwtService.generateToken(user.id);
  res.send({ token });
  return res.end();
});

router.post('/register', registerValidation, async (req, res) => {
  const { password, ...args } = req.body;
  const data = { ...args, password: hashSync(password) };
  const user = await User.create(data);
  const token = JwtService.generateToken(user.id);

  res.send({ token });
  return res.end();
});

router.get('/me', jwtMiddleware, async (req, res) => {
  const { id } = req.user;
  const user = await User.findById(id);

  res.send(user);
  return res.end();
});

router.put('/me', jwtMiddleware, userUpdateValidation, async (req, res) => {
  const { current_password = null, new_password = null, ...args } = req.body;
  const { id } = req.user;
  const user = await User.findById(id);

  const data = {
    ...args,
  };

  // eslint-disable-next-line camelcase
  if (current_password && !compareSync(current_password, user.password)) {
    return res.status(400).send('Wrong password');
  }

  // eslint-disable-next-line camelcase
  if (current_password && compareSync(current_password, user.password)) {
    data.password = hashSync(new_password);
  }

  const newUser = await user.update(data);
  res.send(newUser);
  return res.end();
});

module.exports = router;
