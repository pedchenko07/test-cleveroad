const { Router } = require('express');
const { promises } = require('fs');
const {
  jwtMiddleware,
  itemSearchValidation,
  imageSingleMiddleware,
} = require('../middleware');
const { ImageHelper } = require('../helpers');
const { Item } = require('../models');

const router = new Router();

router.post('/', jwtMiddleware, async (req, res) => {
  const { id } = req.user;
  const { title, price } = req.body;
  const image = ImageHelper.defaultImagePath;
  const { id: newItemId } = await Item.create({
    title,
    price,
    image,
    user_id: id,
  });
  const item = await Item.getItemById(newItemId);

  res.send(item);
  res.end();
});

router.get('/:id', async (req, res) => {
  const { id } = req.params;
  const item = await Item.getItemById(id);
  if (!item) {
    res.status(404).send();
    return res.end();
  }

  res.send(item);
  return res.end();
});

router.get('/', itemSearchValidation, async (req, res) => {
  const items = await Item.search(req.query);

  res.send(items);
  res.end();
});

router.put('/:id', jwtMiddleware, async (req, res) => {
  const { id } = req.params;
  const { title, price } = req.body;

  if (!title && !price) {
    return res
      .status(400)
      .send()
      .end();
  }

  const item = await Item.updateById(id, { title, price });
  res.send(item);
  return res.end();
});

router.post(
  '/:id/image',
  jwtMiddleware,
  imageSingleMiddleware,
  async (req, res) => {
    const { id } = req.params;
    const item = await Item.updateById(id, { image: req.file.path });

    res.send(item);
    res.end();
  },
);

router.delete('/:id/image', jwtMiddleware, async (req, res) => {
  const { id } = req.params;
  const item = await Item.findById(id);
  await promises.unlink(item.image);
  await item.update({ image: ImageHelper.defaultImagePath });

  res.send();
  res.end();
});

module.exports = router;
