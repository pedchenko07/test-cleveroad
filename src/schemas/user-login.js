const Joi = require('joi');

module.exports = Joi.object().keys({
  password: Joi.string()
    .min(3)
    .max(30)
    .required(),
  email: Joi.string()
    .email({ minDomainAtoms: 2 })
    .required(),
});
