const Joi = require('joi');

module.exports = Joi.object()
  .keys({
    name: Joi.string()
      .min(3)
      .max(50),
    email: Joi.string().email({ minDomainAtoms: 2 }),
    phone: Joi.string().regex(/^\+380[\d]{9}$/),
    current_password: Joi.string()
      .min(3)
      .max(50),
    new_password: Joi.string()
      .min(3)
      .max(50),
  })
  .with('current_password', 'new_password');
