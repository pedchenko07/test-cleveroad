const Joi = require('joi');

module.exports = Joi.object().keys({
  name: Joi.string()
    .min(3)
    .max(50)
    .required(),
  password: Joi.string()
    .min(3)
    .max(30)
    .required(),
  email: Joi.string()
    .email({ minDomainAtoms: 2 })
    .required(),
  phone: Joi.string().regex(/^\+380[\d]{9}$/),
});
