const userRegisterSchema = require('./user-register');
const userLoginSchema = require('./user-login');
const userUpdateSchema = require('./user-update');
const itemSearchSchema = require('./item-search');

module.exports = {
  userRegisterSchema,
  userLoginSchema,
  userUpdateSchema,
  itemSearchSchema,
};
