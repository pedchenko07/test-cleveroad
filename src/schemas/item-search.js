const Joi = require('joi');

module.exports = Joi.object().keys({
  title: Joi.string().optional(),
  user_id: Joi.number().optional(),
  order_by: Joi.string()
    .valid(['price', 'created_at'])
    .default('created_at'),
  order_type: Joi.string()
    .valid(['asc', 'desc'])
    .default('desc'),
});
