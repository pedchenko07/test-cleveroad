const multer = require('multer');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './public/uploads/');
  },
  filename: (req, file, cb) => {
    cb(null, `${Date.now()}-${file.originalname}`);
  },
});

const upload = multer({ storage }).single('file');

module.exports = (req, res, next) => {
  upload(req, res, (err) => {
    if (err) {
      // eslint-disable-next-line no-console
      return res.send(err).end();
    }

    return next();
  });
};
