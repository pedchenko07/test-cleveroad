const Joi = require('joi');
const { userLoginSchema } = require('../schemas');

module.exports = (req, res, next) => {
  Joi.validate(req.body, userLoginSchema, (err) => {
    if (err) {
      return res.send(err).end();
    }

    return next();
  });
};
