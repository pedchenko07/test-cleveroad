const jwt = require('express-jwt');

const { JWT_SECRET } = process.env;

module.exports = jwt({ secret: JWT_SECRET });
