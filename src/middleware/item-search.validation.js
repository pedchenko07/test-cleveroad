const Joi = require('joi');
const { itemSearchSchema } = require('../schemas');

module.exports = (req, res, next) => {
  Joi.validate(req.query, itemSearchSchema, (err) => {
    if (err) {
      return res.send(err).end();
    }

    return next();
  });
};
