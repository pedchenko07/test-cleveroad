const Joi = require('joi');
const { userRegisterSchema } = require('../schemas');

module.exports = (req, res, next) => {
  Joi.validate(req.body, userRegisterSchema, (err) => {
    if (err) {
      return res.send(err).end();
    }

    return next();
  });
};
