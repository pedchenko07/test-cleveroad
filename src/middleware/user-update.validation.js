const Joi = require('joi');

const { userUpdateSchema } = require('../schemas');

module.exports = (req, res, next) => {
  Joi.validate(req.body, userUpdateSchema, (err) => {
    if (err) {
      return res.send(err).end();
    }

    return next();
  });
};
