/* eslint-disable camelcase */
const Model = require('./Model');

class Item extends Model {
  static get includes() {
    return {
      user: {
        // eslint-disable-next-line dot-notation
        model: this.models['User'],
        as: 'user',
        attributes: ['id', 'name', 'email', 'phone'],
      },
    };
  }

  static get attributes() {
    const { DOMAIN } = process.env;
    return [
      'id',
      'title',
      'price',
      [
        this.connection.fn('concat', DOMAIN, this.connection.col('image')),
        'image',
      ],
      [
        this.connection.fn(
          'unix_timestamp',
          this.connection.col('Item.created_at'),
        ),
        'created_at',
      ],
    ];
  }

  static getItemById(id, relations = ['user']) {
    const include = relations.map((relation) => this.includes[relation]);
    return this.findOne({
      include,
      where: { id },
      attributes: this.attributes,
    });
  }

  static search(params, relations = ['user']) {
    const {
      title,
      user_id,
      order_by = 'created_at',
      order_type = 'desc',
    } = params;

    let query = {};
    if (title) {
      query = { ...query, title };
    }

    if (user_id) {
      query = { ...query, user_id };
    }

    const order = [order_by, order_type];
    const include = relations.map((relation) => this.includes[relation]);

    return Item.findAll({
      include,
      order: [order],
      where: query,
      attributes: this.attributes,
    });
  }

  static async updateById(id, data, relations = ['user']) {
    const include = relations.map((relation) => this.includes[relation]);
    const item = await this.findOne({ where: { id }, include });
    return item.update(data);
  }
}

module.exports = Item;
