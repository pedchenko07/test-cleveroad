class ImageHelper {
  static get defaultImagePath() {
    return '/public/uploads/default.jpg';
  }
}

module.exports = ImageHelper;
